
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</head>
<body>

    <div class="container">
    
        <div class="col-lg-12">

            <br><br>
            <h1 class="text-warning text-center"> Display Table Data</h1>
            <br>
            <div class="text-right">
            <button class="btn btn-info btn-lg" type="button"> <a href="insert.php" class="text-white"> Add </a> </button>
            </div>
            <br><br>
            <table class="table table-striped table-hover table-bordered">
            
                <tr class="bg-dark text-white text-center">
                
                    <th>ID</th>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Email</th>
                    <th>Delete</th>
                    <th>Update</th>
                
                </tr>

                <?php

                    include 'conn.php';


                        //$firstname = $_POST['firstname'];
                        //$lastname = $_POST['lastname'];
                        //$email = $_POST['email'];
                        $q = "select * from crudtables ";

                        $query = mysqli_query($con,$q);

                        while($res = mysqli_fetch_array($query)){


                ?>

                <tr class="text-center">
                
                    <td><?php echo $res['id'] ?></td>
                    <td><?php echo $res['firstname'] ?></td>
                    <td><?php echo $res['lastname'] ?></td>
                    <td><?php echo $res['email'] ?></td>
                    <td> <button class="btn btn-danger"> <a href="delete.php?id=<?php echo $res['id'] ?> " class="text-white"> Delete </a> </button> </td>
                    <td> <button class="btn btn-primary"> <a href="update.php?id=<?php echo $res['id'] ?> " class="text-white"> Update </a> </button> </td>
                
                </tr>
                
                <?php
                
                    }

                ?>
            </table>
        
        </div>

    </div>


</body>
</html>










